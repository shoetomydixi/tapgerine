<?php

/*

	Proceed all requests

*/
class RequestHandler {
  
    public function __construct() {
        
    }

	/*

		Send http request
	
	*/
    public function sendHttpsRequest($postfields) {

    	$return = array();
    	$requestArray = $postfields["request"];
 
    	while(count($requestArray) > 0) {

    		$request = array_shift($requestArray);

			$return[] = $this->simpleHttpsRequest($postfields["url"], $request);			

    	}

    	return $return;

    }

    /*

		Send simple POST HTTPS request

	*/
    public function simpleHttpsRequest($url, $postfields) {

    	$parsedData;
        
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		// Edit: prior variable $postFields should be $postfields;
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only!
		$result = curl_exec($ch);

		if ($result) {

			$parsedData = json_decode($result);

		}

		return $parsedData;
        
    }



}