<?php

/*

    Format request for API

*/
class RequestFormatter {
       
    public function __construct() {}

    /*

        genearate random string

    */
    public function generateRandomString($length = 10) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;

    }

    /*

        get random player name

    */
    public function getRandomPlayerName() {

        return $this->generateRandomString(3);    	

    }

    /*

    */
    public function getAccountsPlayers($inputData) {

        $requestBlock = [
            "application_id" => "demo",
            "search" => $this->getRandomPlayerName(),
            "limit" => 100
        ];

    	$requestData = [
    		"url" => "https://api.worldoftanks.ru/wot/account/list/",
    		"method" => "POST",
    		"request" => [
                $requestBlock
            ],
            "requestType" => "simple"
    	];

        if (array_key_exists("teamSize", $inputData)) {

            if ($inputData["teamSize"] > 100) {

                $requestData["requestType"] = "recursive";

                $teamSize = $inputData["teamSize"];

                $requestData["request"] = array();

                while ($teamSize > 100) {

                    $requestBlock["search"] = $this->getRandomPlayerName();
                    $requestData["request"][] = $requestBlock;
                    $teamSize = $teamSize - 100;

                }

                $requestBlock["limit"] = $teamSize;
                $requestBlock["search"] = $this->getRandomPlayerName();
                $requestData["request"][] = $requestBlock;

            }else {

                $requestData["request"][0]["limit"] = $inputData["teamSize"];                

            }

        }

    	return $requestData;
         
    }

    /*

    */
    public function getAccountsTechnics($inputData) {

        $requestBlock = [
            "application_id" => "demo",
            "account_id" => ""
        ];

        $requestData = [
            "url" => "https://api.worldoftanks.ru/wot/account/tanks/",
            "method" => "POST",
            "request" => []
        ];

        foreach ($inputData as $key => $value) {
            
            $requestBlock["account_id"] = implode(",", $value);
            $requestData["request"][] = $requestBlock;

        }

        return $requestData;
        
    }

    /*

    */
    public function getParametersTechnics($inputData, $arrayData) {

        $counter = $inputData["MinLevel"];
        $tier = [];

        while($counter <= $inputData["MaxLevel"]) {

            $tier[] = $counter;                
            $counter++;                

        }

        $requestBlock = [
            "application_id" => "demo",
            "tank_id" => "",
            "tier" => implode(",", $tier)
        ];

		$requestData = [
    		"url" => "https://api.worldoftanks.ru/wot/encyclopedia/vehicles/",
    		"method" => "POST",
    		"request" => []
    	];

        foreach ($arrayData as $key => $value) {
            
            $requestBlock["tank_id"] = implode(",", $value);
            $requestData["request"][] = $requestBlock;

        }

        // var_dump($requestData);

    	return $requestData;
        
    }
  
}