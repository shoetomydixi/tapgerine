<?php

require_once './lib/api_processor.php';

/*

    Runner workflow

*/
class Runner {

    /*

        parse and validate input parameters

    */
    public static function getInputArgs($argv) {

    	$proceedParameters = self::validateInputArgs($argv);

    	$ApiProcessor = new ApiProcessor();

    	$ApiProcessor->proceedRequests($proceedParameters);

    }

    /*

        validation for input parameters

    */
    public static function validateInputArgs($argv) {

    	$proceedParameters = array();

    	if (!empty($argv)) {

    		if (!empty($argv[1]) && $argv[1] > 0) {

				$proceedParameters['teamSize'] = intval($argv[1]);

    		}else
    			return ["error" => "Specify size of users to be proceed"]; 

			if (!empty($argv[2]) && $argv[2] > 0) {

				$proceedParameters['MinLevel'] = intval($argv[2]);

    		}else
    			return ["error" => "Specify minimum level"]; 

            if (!empty($argv[3]) && $argv[3] > 0) {

                $proceedParameters['MaxLevel'] = intval($argv[3]);

            }else
                return ["error" => "Specify maximum level"]; 

            if (!empty($argv[2]) && !empty($argv[3]) && $argv[2] >= $argv[3]) 
                return ["error" => "Minimum level cant be greater or equal maximum"];                 

			if (!empty($argv[4]) && $argv[4] > 0) 
                $proceedParameters['clusteringType'] = intval($argv[4]);

			return $proceedParameters;

    	}else 
    		return ["error" => "Specify input paraeters please"];
    }

}