<?php

require_once 'php-kmeans-master/KMeans.php';

/*

    Process results

*/
class ResultProcessor {

    // save accounts players result
    public $AccountsPlayersResult;
    // save accounts technics result
    public $AccountsTechnicsResult;
    // save accounts parameters result
    public $ParametersTechnicsResult;

    // formatted technics
    public $FormattedTechnics;
    // formatted technics
    public $FormattedUsers;

    public function __construct() {}

    /*

        setter for parameters technics result

    */
    public function setParametersTechnicsResult($inputData) {
       
        $this->ParametersTechnicsResult = $inputData;

        return $this->ParametersTechnicsResult;

    }

    /*

        format parameters of technics result

    */
    public function formatParametersTechnicsResult($inputData) {

        $tanksInfo = [];

        foreach ($this->ParametersTechnicsResult as $k => $dataStack) {

            foreach ($dataStack->data as $tankId => $tankInfo) {

                if (isset($tankInfo->tier) && $tankInfo->tier != null) {

                    $tanksInfo[$tankId] = [
                        "name" => $tankInfo->short_name,
                        "tier" => $tankInfo->tier
                    ];

                }

            }

        }

        $this->FormattedTechnics = $tanksInfo;

        return $this->FormattedTechnics;

    }

    public function getDirtyClusters($inputData) {

        $kmeans = new KMeans();

        $numOfclusters = intval(($inputData["MaxLevel"] - $inputData["MinLevel"]) / 2);

        $teamsByClusters = array();

        // 
        // switch clustering parameters 
        // 
        if (isset($inputData["clusteringType"]) && $inputData["clusteringType"] > 1) {

            $kmeans->setData($this->FormattedUsers)
            ->setXKey('markOfMystery')
            ->setYKey('tankTier')
            ->setClusterCount($numOfclusters)
            ->solve();

        }else {

            $kmeans->setData($this->FormattedUsers)
            ->setXKey('xKey')
            ->setYKey('tankTier')
            ->setClusterCount($numOfclusters)
            ->solve();

        }

        $clusters = $kmeans->getClusters();

        echo("num of clusters ".$numOfclusters."\r\n");
        echo("num of users.techincs variations ".count($this->FormattedUsers)."\r\n");

        foreach ($clusters as $clusterNum => $cluster) {

            $teamsByClusters[$clusterNum] = array();

            $x = $cluster->getX();
            $y = $cluster->getY();
            echo(" - cluster number  - ".$clusterNum." - centroid of markOfMystery: ".$cluster->getX()."; centroid of tankTier:" .$cluster->getY()."; total variations in cluster:".count($cluster->getData())." \r\n");

            foreach ($cluster->getData() as $key => $value) {
                
                if (!array_key_exists($value["playerId"], $teamsByClusters[$clusterNum]))
                        $teamsByClusters[$clusterNum][$value["playerId"]] = $value;

            }

            if (count($teamsByClusters[$clusterNum])) {

                echo(" - quantity of uniq users in cluster: ".count($teamsByClusters[$clusterNum])." \r\n");

                $arrayLength = 30;
                $halfOfCluster = 0;

                if (count($teamsByClusters[$clusterNum]) < 30) {

                    $halfOfCluster = intval(count($teamsByClusters[$clusterNum]) / 2); 
                    $arrayLength = $halfOfCluster * 2;

                }else
                    $halfOfCluster = 15;

                $drawedArray = array_slice($teamsByClusters[$clusterNum], 0, $arrayLength);

                if (is_array($drawedArray) && count($drawedArray) && $halfOfCluster > 0) {

                    $arraysToPrint = array_chunk($drawedArray, $halfOfCluster);

                    foreach ($arraysToPrint as $teamNumber => $teamPlayers) {

                        echo(" -- Team number: ".($teamNumber+1)." \r\n");

                        foreach ($teamPlayers as $number => $val) {

                            echo(" --- Number: ".$number."; player: ".$val["playerId"]."; tank:".$val["tankName"]."; tank tier:".$val["tankTier"]."; mark of mystery:".$val["markOfMystery"]." \r\n");

                        }

                    }

                }else {

                    echo ("\r\n drawed array is empty \r\n");

                }  

            }else {



            }


        }

    }

    /*

        Draw teams

    */
    public function renderTeams($clusterNum, $clusterValue) {

        
    } 



    /*

        format users

    */
    public function formatUsers($inputData) {

        foreach ($this->AccountsTechnicsResult as $j => $dataStack) {

            foreach ($dataStack->data as $playerId => $playerTechnics) {
                
                if (!empty($playerTechnics)) {

                    foreach ($playerTechnics as $key => $tankInfo) {
                        
                        if (isset($this->FormattedTechnics[$tankInfo->tank_id])) {

                            $formattedKey = $playerId.",".$tankInfo->tank_id;

                            // $this->FormattedUsers[$formattedKey] = [
                            //     "playerId" => $playerId,
                            //     "tankId" => $tankInfo->tank_id,
                            //     "playerName" => "",
                            //     "markOfMystery" => $tankInfo->mark_of_mastery,
                            //     "tankName" => $this->FormattedTechnics[$tankInfo->tank_id]["name"],
                            //     "tankTier" => $this->FormattedTechnics[$tankInfo->tank_id]["tier"]
                            // ];

                            $this->FormattedUsers[] = [
                                "playerId" => $playerId,
                                "tankId" => $tankInfo->tank_id,
                                "playerName" => "",
                                "playerAlias" => $formattedKey,
                                "markOfMystery" => $tankInfo->mark_of_mastery,
                                "tankName" => $this->FormattedTechnics[$tankInfo->tank_id]["name"],
                                "tankTier" => $this->FormattedTechnics[$tankInfo->tank_id]["tier"],
                                "xKey" => 1
                            ];

                        }

                    }

                }   

            } 

        }

        return $this->FormattedUsers;

    }

    /*

        setter for AcoountsTechnics

    */
    public function setAccountsTechnicsResult($inputData) {
       
        $this->AccountsTechnicsResult = $inputData;

        return $this->AccountsTechnicsResult;

    }

    /*

        get list of uniq technics

    */
    public function getTechnicsUniqList() {
      
        $technicsUniqList = [];

        foreach ($this->AccountsTechnicsResult as $j => $dataStack) {

            foreach ($dataStack->data as $playerId => $playerTechnics) {
                
                if (!empty($playerTechnics)) {

                    foreach ($playerTechnics as $key => $tankInfo) {
                        
                        if (!in_array($tankInfo->tank_id, $technicsUniqList)) {

                            $technicsUniqList[] = $tankInfo->tank_id;

                            // $formattedKey = $playerId.",".$tankInfo->tank_id

                            // $this->FormattedTechnics[$formattedKey] = [
                            //     "playerId" => $playerId,
                            //     "tankId" => $tankInfo->tank_id,
                            //     "playerName" => "",
                            //     "markOfMystery" => $tankInfo->mark_of_mastery
                            // ];

                        }

                    }

                }   

            } 

        }

        // due to limit of tanks list
        $technicsUniqLists = array_chunk($technicsUniqList, 100);

        return $technicsUniqLists;

    }

    /*

        setter for accounts players

    */
    public function setAccountsPlayersResult($inputData) {

    	$this->AccountsPlayersResult = $inputData;

        return $this->AccountsPlayersResult;

    }

    /*

        get formatted list of players
        @return <id>,<id>...,<id>

    */
    public function getAccountsPlayersList() {
      
        $playersList = [];

        foreach ($this->AccountsPlayersResult as $k => $dataStack) {
        
            foreach ($dataStack->data as $j => $player) {
        
                if (!in_array($player->account_id, $playersList)) 
                    $playersList[] = $player->account_id;

            }

        }

        $playersLists = array_chunk($playersList, 100);

        return $playersLists;

    }

}