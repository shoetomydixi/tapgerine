<?php

require_once 'request_formatter.php';
require_once 'request_handler.php';
require_once 'result_processor.php';

/*

    API wokflow controller

*/
class ApiProcessor {

    public $RequestFormatter;
    public $RequestHandler;
    public $ResultProcessor;

    /*

        Init

    */
    public function __construct() {    
    
        $this->RequestFormatter = new RequestFormatter();
        $this->RequestHandler = new RequestHandler();
        $this->ResultProcessor = new ResultProcessor();

    }

    /*

        workflow of balancer 

    */
    public function proceedRequests($inputData) {

        // var_dump($inputData);

        // 
        //  get random players list 
        // 
        $this->ResultProcessor->setAccountsPlayersResult($this->RequestHandler->sendHttpsRequest($this->RequestFormatter->getAccountsPlayers($inputData), array()));

        // 
        //  get technics list by players id list
        // 
        $this->ResultProcessor->setAccountsTechnicsResult($this->RequestHandler->sendHttpsRequest($this->RequestFormatter->getAccountsTechnics($this->ResultProcessor->getAccountsPlayersList())));
           
        // 
        // get technics parameters by tanks id
        // 
        $this->ResultProcessor->setParametersTechnicsResult($this->RequestHandler->sendHttpsRequest($this->RequestFormatter->getParametersTechnics($inputData, $this->ResultProcessor->getTechnicsUniqList())));
        
        // 
        // format technics result 
        // 
        $this->ResultProcessor->formatParametersTechnicsResult($inputData);

        // 
        // format users for clustering 
        // 
        $this->ResultProcessor->formatUsers($inputData);

        // 
        // format clusters 
        // 
        $this->ResultProcessor->getDirtyClusters($inputData);

    }

}